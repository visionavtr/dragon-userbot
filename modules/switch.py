# Code is licensed under CC-BY-NC-ND 4.0 unless otherwise specified.
# https://creativecommons.org/licenses/by-nc-nd/4.0/
# You CANNOT edit this file without direct permission from the author.
# You can redistribute this file without any changes.
from pyrogram import Client, filters
from pyrogram.types import Message

from utils.misc import modules_help, prefix

ru = """ёйцукенгшщзхъфывапролджэячсмитьбю.Ё"№;%:?ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭ/ЯЧСМИТЬБЮ,"""
en = """`qwertyuiop[]asdfghjkl;'zxcvbnm,./~@#$%^&QWERTYUIOP{}ASDFGHJKL:"|ZXCVBNM<>?"""


@Client.on_message(filters.command("sw", prefix) & filters.me)
async def say(_, message: Message):
    if message.reply_to_message:
        text = message.reply_to_message.text
        if not text:
            return await message.edit("No text found.")

        change = str.maketrans(ru + en, en + ru)
        text = text.translate(change)

        if message.reply_to_message.from_user.id != message.from_user.id:
            await message.edit(text)
        else:
            await message.delete()
            await message.reply_to_message.edit(text)

    else:
        text = message.text.split(maxsplit=1)
        if len(text) == 1:
            return await message.edit("No text found.")
        text = text[1]

        change = str.maketrans(ru + en, en + ru)
        text = text.translate(change)

        await message.edit(text)


modules_help["switch"] = {
    "sw [command]*": "Switch keyboard layout.",
}
